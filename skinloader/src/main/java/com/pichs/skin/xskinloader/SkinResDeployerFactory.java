package com.pichs.skin.xskinloader;

import android.text.TextUtils;

import com.pichs.skin.xskinloader.entity.SkinAttr;
import com.pichs.skin.xskinloader.skinDeployer.ActivityNavigationBarColorResDeployer;
import com.pichs.skin.xskinloader.skinDeployer.CardViewBackgroundColorResDeployer;
import com.pichs.skin.xskinloader.skinInterface.ISkinResDeployer;
import com.pichs.skin.xskinloader.skinDeployer.ActivityStatusBarColorResDeployer;
import com.pichs.skin.xskinloader.skinDeployer.ListViewDividerResDeployer;
import com.pichs.skin.xskinloader.skinDeployer.ListViewSelectorResDeployer;
import com.pichs.skin.xskinloader.skinDeployer.ProgressBarIndeterminateDrawableDeployer;

import java.util.HashMap;

/**
 * 皮肤工厂
 */
public class SkinResDeployerFactory {

    public static final String TEXT_COLOR = "textColor";
    public static final String LIST_SELECTOR = "listSelector";
    public static final String DIVIDER = "divider";
    // CardView
    public static final String CARD_BACKGROUND_COLOR = "cardBackgroundColor";

    public static final String ACTIVITY_STATUS_BAR_COLOR = "statusBarColor";
    public static final String ACTIVITY_NAVIGATION_BAR_COLOR = "navigationBarColor";
    public static final String PROGRESSBAR_INDETERMINATE_DRAWABLE = "indeterminateDrawable";

    //存放支持的换肤属性和对应的处理器
    private static final HashMap<String, HashMap<Class<? extends ISkinResDeployer>, ISkinResDeployer>> sDefaultDeployerMap = new HashMap<>();

    //静态注册支持的属性和处理器
    static {
        registerDeployer(CARD_BACKGROUND_COLOR, new CardViewBackgroundColorResDeployer());
        registerDeployer(LIST_SELECTOR, new ListViewSelectorResDeployer());
        registerDeployer(DIVIDER, new ListViewDividerResDeployer());
        registerDeployer(ACTIVITY_STATUS_BAR_COLOR, new ActivityStatusBarColorResDeployer());
        registerDeployer(ACTIVITY_NAVIGATION_BAR_COLOR, new ActivityNavigationBarColorResDeployer());
        registerDeployer(PROGRESSBAR_INDETERMINATE_DRAWABLE, new ProgressBarIndeterminateDrawableDeployer());
    }

    public static void registerDeployer(String attrName, ISkinResDeployer skinResDeployer) {
        if (TextUtils.isEmpty(attrName) || null == skinResDeployer) {
            return;
        }
        HashMap<Class<? extends ISkinResDeployer>, ISkinResDeployer> map;
        if (sDefaultDeployerMap.get(attrName) == null) {
            map = new HashMap<>();
            map.put(skinResDeployer.getClass(), skinResDeployer);
            sDefaultDeployerMap.put(attrName, map);
        } else {
            map = sDefaultDeployerMap.get(attrName);
            map.put(skinResDeployer.getClass(), skinResDeployer);
        }
    }

    public static HashMap<Class<? extends ISkinResDeployer>, ISkinResDeployer> of(SkinAttr attr) {
        if (attr == null) {
            return null;
        }
        return of(attr.attrName);
    }

    public static HashMap<Class<? extends ISkinResDeployer>, ISkinResDeployer> of(String attrName) {
        if (TextUtils.isEmpty(attrName)) {
            return null;
        }
        return sDefaultDeployerMap.get(attrName);
    }

    public static boolean isSupportedAttr(String attrName) {
        return of(attrName) != null && !of(attrName).isEmpty();
    }

}
