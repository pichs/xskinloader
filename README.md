# XSkinLoader



最新版本：[![](https://jitpack.io/v/com.gitee.pichs/xskinloader.svg)](https://jitpack.io/#com.gitee.pichs/xskinloader)



### 引入

      ```
      
        implementation 'com.gitee.pichs:xskinloader:1.1'
        
        
      ```

### 用法

- 可以参考： https://github.com/WindySha/XSkinLoader
- 基本一样。

```
      <?xml version="1.0" encoding="utf-8"?>
      <RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
          xmlns:app="http://schemas.android.com/apk/res-auto"
          android:layout_width="match_parent"
          android:layout_height="match_parent"

          // 使用这个命名空间 在根布局中-------------->
------>   xmlns:skin="http://schemas.android.com/android/skin"
   


          增加skin命名空间

          <TextView

            // 使用换肤属性
            skin:enable="true"/>
          
          </>
            
            
```

### 框架改动
    
- 在此框架基础上增加CardView背景属性换肤 <br>开放从皮肤资源包中获取Color的方法，方便代码使用。
- 此框架必须配合 *** [xwidget](https://gitee.com/pichs/xwidget) *** 使用，<br>否则需要您自己实现背景颜色，字体颜色等换肤扩展<br>
  实现方式后期再写文档吧，xwidget解决你的一切布局烦恼。

- 此次更新优化换肤注册原理，支持一个属性多个换肤扩展（这一点更符合真实需求）
  在大批量自定义控件项目中，想要只用一种属性写一个扩展类，那应该会累死，而且还无法解耦其他相同属性库<br>
  专门优化了这块，扩展库写起来更爽了，更顺了。
  
- 此框架还会为 xuikit 框架的换肤扩展做基础，
- 我强烈建议您使用xwidget，xuikit这两个库，这是我的心血力作，优化了无数个版本，<br>一点一点打磨，只为更快捷的开发而设计,库还会持续优化中
- 为了保证稳定性，所以将此库搬运下来，自己维护。


### 感谢
- 换肤方案， 参考github大神的[XSkinLoader](https://github.com/WindySha/XSkinLoader) ，并做了一些基础ui的支持<br>
个人还是比较喜欢这种低侵入性的换肤框架：[XSkinLoader](https://github.com/WindySha/XSkinLoader)  我是一个代码搬运工。<br>
 [XSkinLoader](https://github.com/WindySha/XSkinLoader) 提供的支持
 
- 点个star支持一下我吧！<br>